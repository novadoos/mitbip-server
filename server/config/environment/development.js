'use strict';

// Development specific configuration
// ==================================
module.exports = {
  // MongoDB connection options
  mongo: {
    uri: 'mongodb://192.168.99.100:27017/mitbip-dev'
  },

  facebook: {
    clientID: '1606381282958200',
    clientSecret: 'f349fa8dcb2ab95048f891476f9be956'
  },

  providers:{
    local: 'local'
  },

  roles:{
    user: 'user'
  },

  chatStates:{
    online: 'online',
    waiting: 'waiting',
    busy: 'busy'
  },

  imageFormat: '.jpeg',
  avatarFolder: '/client/public/users/avatar/',
  defaultAvatarName: 'default',
  avatarWidth: '200',
  avatarHeight: '200',

  seedDB: false
};
