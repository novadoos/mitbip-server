'use strict';

var _ = require('lodash');
var User = require('./user.model');
var FB = require('fb');
var Logger = require('../../service/logger');
var email = require('./user.email');
var logger = Logger.createLog().child({component: 'users'});
var async = require('async');
var config = require('../../config/environment');
var lib = require('./user.lib.js');
var request = require('request');
var path = require('path');
var fs = require('fs');

var validationTrace = function(res, err, message, status) {
  if (!err && message) err = new Error(message);
  if ([500, 408, 400].indexOf(status) > -1) {
    logger.error({err: err}, message);
    if (res && res.json) return res.json(status, err);
  } else if ([200, 201, 204].indexOf(status) > -1) {
    logger.info(message);
  } else if ([401].indexOf(status) > -1) {
    logger.warn(message);
    if (res && res.json) return res.json(status, err);
  }
};

exports.index = function(req, res) {
  User.find(function (err, users) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(users);
  });
};

// Get a single user
exports.show = function(req, res) {
  User.findById(req.params.id, function (err, user) {
    if(err) { return handleError(res, err); }
    if(!user) { return res.status(404).send('Not Found'); }
    return res.json(user);
  });
};

// Creates a new user in the DB.
exports.create = function(req, response) {
  FB.setAccessToken(req.body.access_token);

  FB.api('/me', {fields: ['email', 'name', 'birthday', 'link', 'gender', 'timezone', 'locale', 'location']}, function(res) {
    if (!res.email) {
      FB.api('me/permissions', 'DELETE', function(res) {
        if (res.error) return validationTrace(response, res.error, 'There is a problem deleting facebook permissions on createFromFacebook method', 500);
        return validationTrace(response, null, 'No facebook email permissions on createFromFacebook method', 500);
      });
    } else {
      if (res && !res.error) {
        if (res.id) {
          User.findOne({facebookId: res.id}, '-salt -hashedPassword', function(err, user) {
            if (err) return validationTrace(response, err, 'There is a problem with User.findOne on createFromFacebook method', 500);
            if (user) {
              FB.api('/me',
                {access_token: user.facebookLToken},
                function(res) {
                  if (!res)
                    return validationTrace(response, null, 'No facebook response on method createFromFacebook', 500);
                  if (res.error) {
                    validationTrace(null, res.error, 'Facebook error on createFromFacebook method', 500);

                    FB.api('oauth/access_token', {
                        client_id: config.facebook.clientID,
                        client_secret: config.facebook.clientSecret,
                        grant_type: 'fb_exchange_token',
                        fb_exchange_token: req.body.access_token
                      },
                      function(ftRes) {
                        if (!ftRes)
                          return validationTrace(response, null, 'No facebook response for ouath/accessToken on method createFromFacebook', 500);
                        if (ftRes.error)
                          return validationTrace(response, ftRes.error, 'Facebook error for ouath/accessToken on createFromFacebook method', 500);

                        user.facebookLToken = ftRes.access_token;

                        validationTrace(null, null, 'Success on createFromFacebook method', 200);
                        return response.json(200, user);
                      });
                  } else {
                    validationTrace(null, null, 'Success on createFromFacebook method', 200);
                    return response.json(200, user);
                  }
                }
              );
            } else {
              var newUser = new User();
              async.series({
                createFacebookLongToken: function(callback) {
                  FB.api('oauth/access_token', {
                      client_id: config.facebook.clientID,
                      client_secret: config.facebook.clientSecret,
                      grant_type: 'fb_exchange_token',
                      fb_exchange_token: req.body.access_token
                    },
                    function(ftRes) {
                      if (!ftRes)
                        return callback(new Error('No facebook response for ouath/accessToken on method createFromFacebook'));
                      if (ftRes.error)
                        return callback(ftRes.error);

                      newUser.facebookLToken = ftRes.access_token;
                      callback(null, newUser);
                    });
                },
                createBaseUser: function(callback) {
                  newUser.provider = config.providers.local;
                  newUser.role = config.roles.user;
                  newUser.name = res.name ? res.name : '';
                  newUser.gender = res.gender ? res.gender : '';
                  newUser.facebookId = res.id ? res.id : '';
                  newUser.facebookLink = res.link ? res.link : '';
                  newUser.timezone = res.timezone;
                  newUser.locale = res.locale ? res.locale : '';
                  newUser.location = res.location ? res.location : '';
                  newUser.password = lib.generatePassword();
                  newUser.email = res.email;
                  newUser.enabled = true;

                  newUser.save(function(err, userSaved) {
                    if (err) return callback(err);
                    if (!userSaved) return callback(new Error('no user saved on createBaseUser'));
                    callback(null, userSaved);
                  });
                },
                setUserAvatar: function(callback) {
                  FB.api('/me/picture',
                    {
                      redirect: false,
                      height: config.avatarHeight,
                      type: 'normal',
                      width: config.avatarWidth
                    },
                    function(res) {
                      if (res && !res.error) {
                        if (res.data) {
                          if (res.data.is_silhouette) {
                            newUser.avatar = path.join
                            (
                              config.root + config.avatarFolder +
                                newUser.defaultAvatarName + config.imageFormat
                            );
                            callback(null, newUser);
                          } else {
                            request.get(res.data.url)
                              .on('error', function(err) {
                                return callback(err);
                              })
                              .on('response', function() {
                                newUser.avatar = path.join
                                (
                                  config.root + config.avatarFolder +
                                  newUser._id + config.imageFormat
                                );
                                callback(null, newUser);
                              })
                              .pipe(
                              fs.createWriteStream(
                                path.join(config.root + config.avatarFolder) +
                                newUser._id + config.imageFormat)
                            );
                          }
                        } else {
                          return callback(new Error('No data were given'));
                        }
                      } else {
                        if (res.error.code === config.errorCodes.ETIMEDOUT) {
                          return callback(res.error);
                        } else {
                          return callback(res.error);
                        }
                      }
                    }
                  );
                },
                updateAvatarAndSendEmail: function(callback) {
                  var userToUpdate = newUser.toObject();
                  User.update({username: newUser.username}, userToUpdate, function(err, userSaved) {
                    if (err) return callback(err);
                    if (!userSaved) return callback(new Error('No user saved on updateAvatarAndSendEmail'));

                    email.welcome(newUser, function(error) {
                      if (error) return callback(error);
                      callback(null, newUser);
                    });
                  });
                }
              }, function(errors, results) {
                function deleteUser(error) {
                  if (newUser && newUser._id) {
                    var userToRemove = newUser;
                    User.findByIdAndRemove(newUser._id, function(removeErr) {
                      if (fs.existsSync(userToRemove.avatar)) fs.unlinkSync(userToRemove.avatar);
                      if (removeErr) return validationTrace(response, removeErr, 'Error deleting user on createFromFacebook method', 500);
                      return validationTrace(response, error, 'Errors on createFromFacebook method', 500);
                    });
                  } else {
                    return validationTrace(response, error, 'Errors on createFromFacebook method', 500);
                  }
                }

                if (errors) deleteUser(errors);
                if (!results) deleteUser('No user saved on final function async');

                validationTrace(null, null, 'Success on createFromFacebook method', 201);
                return response.json(201, results.updateAvatarAndSendEmail);
              });
            }
          });
        }
      } else {
        if (res.error) {
          if (res.error.code === config.errorCodes.ETIMEDOUT) {
            return validationTrace(response, res.error, 'ETIMEDOUT on createFromFacebook method', 408);
          } else {
            return validationTrace(response, res.error, 'Error for facebook response on createFromFacebook method', 500);
          }
        }
      }
    }
  });
};

// Updates an existing user in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  User.findById(req.params.id, function (err, user) {
    if (err) { return handleError(res, err); }
    if(!user) { return res.status(404).send('Not Found'); }
    var updated = _.merge(user, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(user);
    });
  });
};

// Deletes a user from the DB.
exports.destroy = function(req, res) {
  User.findById(req.params.id, function (err, user) {
    if(err) { return handleError(res, err); }
    if(!user) { return res.status(404).send('Not Found'); }
    user.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}