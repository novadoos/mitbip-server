'use strict';

var config = require('../../config/environment');
var lib = require('./chat.lib');

global.allUsers = [];

function connectPartners(socket, socketio, user) {
  var myInfo = {socketId: socket.id, username: user.username};
  var partner;
  var partnerSocket;

  if (global.allUsers.length > 0) {
    var randomNumber = lib.generateRandomNumber(global.allUsers.length - 1);
    partner = global.allUsers[randomNumber];

    // We do this so users won't connect to the same room or the same account
    while (!partner || partner.socketId === myInfo.socketId || partner.username === myInfo.username) {
      randomNumber = lib.generateRandomNumber(global.allUsers.length - 1);
      partner = global.allUsers[randomNumber];
    }

    // Remove the partner we found from the list of solo users
    global.allUsers.splice(randomNumber, 1);

    // Remove my object from the list of solo users
    for (var i = 0; i < global.allUsers.length; i++) {
      if (global.allUsers[i].socketId === myInfo.socketId) {
        global.allUsers.splice(i, 1);
        break;
      }
    }
  }

  if (partner) {
    partnerSocket = socketio.sockets.connected[partner.socketId];

    // Join to the each other room
    socket.join(partnerSocket);
    partnerSocket.join(socket);

    // Mark that my new partner and me are partners
    socket.partner = partner;
    partnerSocket.partner = myInfo;

    // Mark that we are busy chatting with someone
    socket.status = config.chatStates.busy;
    partnerSocket.status = config.chatStates.busy;

    // Set 'partner has connected' mesasge on each other room
    socket.emit('newPartner', 'Partner has connected');
    partnerSocket.emit('newPartner', 'Partner has connected');

    // Update each other partner information
    partnerSocket.emit('updatePartnerInfo', user);

  } else {
    // delete that i had a partner if i had one
    if (socket.partner) delete socket.partner;

    // add myself to the list of solo users if i'm not in the list
    if (socket.status !== config.chatStates.online) {
      socket.status = config.chatStates.online;
      global.allUsers.push(myInfo);
    }
  }
}

function onIncludeUser(socket, socketio, user) {
  if (user && user.username) {
    socket.username = user.username;

    connectPartners(socket, socketio, user);
  }
}

function onDisconnectPartners(socket, socketio, user) {
  if (socket.partner && socket.partner.socketId) {
    var partner = socket.partner;
    var partnerSocket = socketio.sockets.connected[partner.socketId];
    if (partnerSocket) {

      //Update partner window with "user leaves message"
      partnerSocket.emit('waitingByUser', 'Partner has disconnected');

      //Put my partner "Waiting" and has no partner anymore
      partnerSocket.status = config.chatStates.waiting;
      partnerSocket.partner = null;
    }
  }

  socket.status = config.chatStates.online;
  socket.partner = null;
  global.allUsers.push({socketId: socket.id, username: socket.username});

  // Tries to connect to another partner
  connectPartners(socket, socketio, user);
}

function onSendChat(socket, socketio, message) {
  socket.emit('updateChat', socket.username, 'me', message);

  if (socket.partner) {
    var partner = socket.partner;
    var partnerSocket = socketio.sockets.connected[partner.socketId];
    if (partnerSocket) {
      partnerSocket.emit('updateChat', socket.username , 'partner', message);
    }
  }
}

exports.register = function(socket, socketio) {
  socket.on('includeUser', function(user) {
    onIncludeUser(socket, socketio, user);
  });

  socket.on('disconnectPartners', function(user) {
    onDisconnectPartners(socket, socketio, user);
  });

  socket.on('sendChat', function(message) {
    onSendChat(socket, socketio, message);
  });
};