'use strict';

exports.generateRandomNumber = function(length) {
  return Math.round(Math.random() * (length));
};