'use strict';

var crypto = require('crypto');

exports.encryptPassword = function(salt, password) {
  if (!password || !salt) return '';
  salt = new Buffer(salt, 'base64');
  return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
};

exports.generatePassword = function() {
  return Math.random().toString(36).slice(-8);
};
